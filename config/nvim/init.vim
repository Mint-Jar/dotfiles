" Install Vim Plug if not installed
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif

" To relead the Vim config enter the following command:
" :so %
call plug#begin()
"Vim help for Vim Plug
Plug 'junegunn/vim-plug'

" Vim recomended default settings as a plugin
Plug 'tpope/vim-sensible'

" GruvBox colorscheme
Plug 'morhetz/gruvbox'

" Lightline
Plug 'itchyny/lightline.vim'

" Ale
Plug 'w0rp/ale'

" Vim Wiki
Plug 'vimwiki/vimwiki'

" Fuzzy File Search
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Nerd tree
Plug 'scrooloose/nerdtree'

" PHP related plugins
Plug 'StanAngeloff/php.vim'

Plug 'roxma/LanguageServer-php-neovim',  {'do': 'composer install && composer run-script parse-stubs'}
call plug#end()

" Enable hybrid relative number
set number relativenumber

" Enable global gruvbox colorscheme
set termguicolors
colorscheme gruvbox
set background=dark

" Easy buffer swithch on f5
nnoremap <F5> :buffers<CR>:buffer<Space>

" Use 4 spaces instead of tab
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

" Lightline settings for Gruvbox
set noshowmode
let g:lightline = { 'colorscheme': 'gruvbox', }

" Ale settings
set completeopt=menu,menuone,preview,noselect,noinsert
let g:ale_php_langserver_use_global = 1
let g:ale_php_langserver_executable = globpath(&rtp,'vendor/felixfbecker/language-server/bin/php-language-server.php',1)
let g:ale_completion_enabled = 1
inoremap <silent> <C-Space> <C-\><C-O>:ALEComplete<CR>

" Nerd tree settings
map <C-n> :NERDTreeToggle<CR>
